package edu.darkcat.lab07_sqlite_indp.view

import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import edu.darkcat.lab07_sqlite_indp.R
import edu.darkcat.lab07_sqlite_indp.database.model.User
import java.text.SimpleDateFormat
import java.util.*

class UserForm : AppCompatActivity() {

    private lateinit var Fullname: EditText
    private lateinit var Email: EditText
    private lateinit var PhoneNumber: EditText
    private lateinit var Password: EditText
    private lateinit var VaccinationDate: EditText
    private lateinit var VacinationType: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_form)

        this.Fullname = findViewById(R.id.fullname)
        this.Email = findViewById(R.id.email)
        this.PhoneNumber = findViewById(R.id.phonenumber)
        this.Password = findViewById(R.id.password)
        this.VaccinationDate = findViewById(R.id.vaccinationDate)
        this.VacinationType = findViewById(R.id.typeVaccine)

        var i: Int = 0

        val formatter = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        val date = formatter.parse(VaccinationDate.text.toString())

        User(
            i,
            Fullname.text.toString(),
            Email.text.toString(),
            PhoneNumber.text.toString(),
            Password.text.toString(),
            date,
            VacinationType.text.toString()
        )

    }
}