package edu.darkcat.lab07_sqlite_indp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import edu.darkcat.lab07_sqlite_indp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn1 = findViewById<Button>(R.id.user_button)
        val btn2 = findViewById<Button>(R.id.department_button)
        val btn3 = findViewById<Button>(R.id.province_button)

        btn1.setOnClickListener{
            val intent = Intent(this, UserForm::class.java)
            startActivity(intent)
        }
        btn2.setOnClickListener{
            val intent = Intent(this, DepartmentForm::class.java)
            startActivity(intent)
        }
        btn3.setOnClickListener{
            val intent = Intent(this, ProvinceForm::class.java)
            startActivity(intent)
        }
    }
}