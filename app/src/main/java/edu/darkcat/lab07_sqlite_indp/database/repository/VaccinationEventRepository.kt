package edu.darkcat.lab07_sqlite_indp.database.repository

import androidx.lifecycle.LiveData
import edu.darkcat.lab07_sqlite_indp.database.dao.VaccinationEventDao
import edu.darkcat.lab07_sqlite_indp.database.model.VaccinationEvent

class VaccinationEventRepository (private val vaccinationEventDao: VaccinationEventDao){

    val getAllVaccinationEvents:LiveData<List<VaccinationEvent>> = vaccinationEventDao.getAll()

    fun getVaccinationEventByUserId(idUser : Int):LiveData<List<VaccinationEvent>>{
        return vaccinationEventDao.getAllVaccinationEventsFromUser(idUser)
    }

    fun insertVaccinationEvent(vaccinationEvent : VaccinationEvent){
        vaccinationEventDao.insertVaccinationEvent(vaccinationEvent)
    }

    fun deleteVaccinationEventByUserId(idUser : Int){
        vaccinationEventDao.deleteAllVaccinationEventsFromUser(idUser)
    }

    fun deleteVaccinationEventByEventId(idEvent : Int){
        vaccinationEventDao.deleteOneVaccinationEventFromId(idEvent)
    }

}