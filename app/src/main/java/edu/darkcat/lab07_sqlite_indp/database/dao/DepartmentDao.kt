package edu.darkcat.lab07_sqlite_indp.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import edu.darkcat.lab07_sqlite_indp.database.model.Department

@Dao
abstract class DepartmentDao {
    @Query("SELECT * FROM Department")
    abstract fun getAll(): LiveData<List<Department>>
}