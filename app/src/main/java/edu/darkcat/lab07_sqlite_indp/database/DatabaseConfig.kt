package edu.darkcat.lab07_sqlite_indp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import edu.darkcat.lab07_sqlite_indp.database.converter.Converters
import edu.darkcat.lab07_sqlite_indp.database.dao.*
import edu.darkcat.lab07_sqlite_indp.database.model.*

@Database(
    entities = [User::class, Department::class, Province::class, VaccinationEvent::class, VaccinationSite::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class DatabaseConfig : RoomDatabase() {
    // Declaration DAOs
    abstract fun userDao(): UserDao
    abstract fun departmentDao(): DepartmentDao
    abstract fun provinceDao(): ProvinceDao
    abstract fun vaccinationEventDao(): VaccinationEventDao
    abstract fun vaccinationSiteDao(): VaccinationSiteDao

    // Database Invocation
    companion object {
        @Volatile
        private var INSTANCE: DatabaseConfig? = null
        fun getDatabase(context: Context): DatabaseConfig {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DatabaseConfig::class.java,
                    "word_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}