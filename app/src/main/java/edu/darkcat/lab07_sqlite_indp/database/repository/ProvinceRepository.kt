package edu.darkcat.lab07_sqlite_indp.database.repository

import androidx.lifecycle.LiveData
import edu.darkcat.lab07_sqlite_indp.database.dao.ProvinceDao
import edu.darkcat.lab07_sqlite_indp.database.model.Province

class ProvinceRepository(private val provinceDao: ProvinceDao) {

    val getAllProvinces: LiveData<List<Province>> = provinceDao.getAll()

}