package edu.darkcat.lab07_sqlite_indp.database.repository

import androidx.lifecycle.LiveData
import edu.darkcat.lab07_sqlite_indp.database.dao.UserDao
import edu.darkcat.lab07_sqlite_indp.database.model.User
import kotlinx.coroutines.flow.Flow

class UserRepository (private val userDao: UserDao){

    val getAllUsers:Flow<List<User>> = userDao.getAllUser()

    fun getUserById(idUser: Int): LiveData<User> {
        return userDao.getUser(idUser)
    }

    fun insertUser(user: User){
        userDao.insertUser(user)
    }

    fun updateUser(vararg user: User){
        userDao.updateUser(*user)
    }

    fun deleteUser(idUser: Int){
        userDao.deleteUser(idUser)
    }
}