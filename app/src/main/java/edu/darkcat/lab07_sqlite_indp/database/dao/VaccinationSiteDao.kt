package edu.darkcat.lab07_sqlite_indp.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import edu.darkcat.lab07_sqlite_indp.database.model.VaccinationSite

@Dao
abstract class VaccinationSiteDao {
    @Query("SELECT * FROM VaccinationSite")
    abstract fun getAll(): LiveData<List<VaccinationSite>>



}