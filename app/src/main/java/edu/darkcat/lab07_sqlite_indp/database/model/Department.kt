package edu.darkcat.lab07_sqlite_indp.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Department(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var nameDepartment: String?
)