package edu.darkcat.lab07_sqlite_indp.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.IGNORE
import edu.darkcat.lab07_sqlite_indp.database.model.User
import kotlinx.coroutines.flow.Flow

@Dao
abstract class UserDao {
    @Query("SELECT * FROM User")
    abstract fun getAllUser(): Flow<List<User>>

    @Query("SELECT * FROM User WHERE id = :idUser")
    abstract fun getUser(idUser: Int): LiveData<User>

    @Insert(onConflict = IGNORE)
    abstract fun insertUser(user: User)

    @Update
    abstract fun updateUser(vararg user: User)

    @Query("DELETE FROM User WHERE id = :idUser")
    abstract fun deleteUser(idUser: Int)

    @Query("DELETE FROM User")
    abstract fun deleteAllUsers()
}