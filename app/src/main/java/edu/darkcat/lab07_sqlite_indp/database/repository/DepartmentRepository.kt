package edu.darkcat.lab07_sqlite_indp.database.repository

import androidx.lifecycle.LiveData
import edu.darkcat.lab07_sqlite_indp.database.dao.DepartmentDao
import edu.darkcat.lab07_sqlite_indp.database.model.Department

class DepartmentRepository (private val departmentDao: DepartmentDao) {

    val getAllDepartments: LiveData<List<Department>> = departmentDao.getAll()

}