package edu.darkcat.lab07_sqlite_indp.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var fullName: String?,
    var phoneNumber: String?,
    var email: String?,
    var password: String?,
    var vaccinationDate: Date?,
    var typeVaccine: String?
){
    companion object{
        val AnonymousUser by lazy{
            User(
                id = 10000,
                fullName = "Anonymous User",
                phoneNumber = "0",
                email = "No E-Mail",
                password = "No Password",
                vaccinationDate = Date(),
                typeVaccine = "Without vaccine"
            )
        }
    }
}

