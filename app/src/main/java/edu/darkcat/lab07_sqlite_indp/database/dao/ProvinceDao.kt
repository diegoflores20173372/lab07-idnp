package edu.darkcat.lab07_sqlite_indp.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import edu.darkcat.lab07_sqlite_indp.database.model.Province

@Dao
abstract class ProvinceDao {
    @Query("SELECT * FROM Province")
    abstract fun getAll(): LiveData<List<Province>>
}