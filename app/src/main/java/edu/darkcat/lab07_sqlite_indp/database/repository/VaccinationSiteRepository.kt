package edu.darkcat.lab07_sqlite_indp.database.repository

import androidx.lifecycle.LiveData
import edu.darkcat.lab07_sqlite_indp.database.dao.VaccinationSiteDao
import edu.darkcat.lab07_sqlite_indp.database.model.VaccinationSite

class VaccinationSiteRepository (private val vaccinationSiteDao: VaccinationSiteDao){

    val getAllVaccinationSite : LiveData<List<VaccinationSite>> = vaccinationSiteDao.getAll()

}