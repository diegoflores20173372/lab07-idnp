package edu.darkcat.lab07_sqlite_indp.viewmodel

import androidx.lifecycle.*
import edu.darkcat.lab07_sqlite_indp.database.model.User
import edu.darkcat.lab07_sqlite_indp.database.repository.UserRepository
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class UserViewModel(private val userRepository: UserRepository): ViewModel() {

    val getAllUsers: LiveData<List<User>> = userRepository.getAllUsers.asLiveData()

    fun insert(user: User) = viewModelScope.launch {
        userRepository.insertUser(user)
    }


}

class UserViewModelFactory(private val userRepository: UserRepository): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UserViewModel(userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }
}